const togglePasswordIcons = document.querySelectorAll(".icon-password");
const togglePassword = (event) => {
    const input = event.target.previousElementSibling;
    const icon = event.target;

    if (input.type === "password") {
        input.type = "text";
        icon.classList.remove("fa-eye");
        icon.classList.add("fa-eye-slash");
    } else {
        input.type = "password";
        icon.classList.remove("fa-eye-slash");
        icon.classList.add("fa-eye");
    }
}

togglePasswordIcons.forEach((icon) => {
    icon.addEventListener("click", togglePassword);
});

const button = document.querySelector(".button");
const passwordInputs = document.querySelectorAll("input[type='password']");
let errorMessage = null;
const comparePasswords = (event) => {
    event.preventDefault();

    if ((passwordInputs[0].value || passwordInputs[1].value) === "") {
        alert("Too short password");
    } else if (passwordInputs[0].value === passwordInputs[1].value) {
        alert("You are welcome");
        if (errorMessage) {
            errorMessage.remove();
            errorMessage = null;
        }
    } else {
        if (!errorMessage) {
            errorMessage = document.createElement("p");
            errorMessage.textContent = "Write the same passwords!";
            errorMessage.style.color = "red";
            errorMessage.style.fontSize = "20px";
            errorMessage.style.position = "relative";
            errorMessage.style.left = "-32px";
            button.parentNode.insertBefore(errorMessage, button);
        }
    }
};

button.addEventListener("click", comparePasswords);

const inputs = document.querySelectorAll(".inputs");
const nextTheme = document.querySelector('.next-theme');
const background = document.body ;
const themeKey = "theme";

const changeTheme = (toggleTheme) => {
  if (background.classList.contains("dark")) {
      background.classList.remove("dark");
      localStorage.setItem(themeKey, "light");
  } else {
      background.classList.add("dark");
      localStorage.setItem(themeKey, "dark");
  }
}

const savedTheme = localStorage.getItem(themeKey);

if (savedTheme) {
    background.classList.add(savedTheme);
}

nextTheme.addEventListener("click", changeTheme);
